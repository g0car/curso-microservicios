const bull = require('bull')
const {
  redis: { host, port },
} = require('../settings')

const options = {
  redis: { host, port },
}

const queueCreate = bull('user:create')
const queueDelete = bull('user:delete', options)
const queueUpdate = bull('user:update', options)
const queueFindOne = bull('user:finOne', options)
const queueViewAll = bull('user:viewAll', options)

module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueViewAll }
