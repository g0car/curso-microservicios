const Services = require('../Services')
const { InternalError } = require('../settings')
const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueViewAll } = require('./index')

queueCreate.process(async (job, done) => {
  try {
    const { user } = job.data
    const { statusCode, data, message } = await Services.Create({ user })
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ adapter: 'queueCreate', error: error.toString() })
    done(null, { statusCode: 500, data: null, message: InternalError })
  }
})
queueDelete.process(async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.Delete({ id })
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ adapter: 'queueDelete', error: error.toString() })
    done(null, { statusCode: 500, data: null, message: InternalError })
  }
})
queueUpdate.process(async (job, done) => {
  try {
    const { user } = job.data
    const { statusCode, data, message } = await Services.Update({ user })
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ adapter: 'queueUpdate', error: error.toString() })
    done(null, { statusCode: 500, data: null, message: InternalError })
  }
})
queueFindOne.process(async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.FindOne({ id })
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ adapter: 'queueFindOne', error: error.toString() })
    done(null, { statusCode: 500, data: null, message: InternalError })
  }
})
queueViewAll.process(async (job, done) => {
  try {
    const { statusCode, data, message } = await Services.ViewAll()
    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ adapter: 'queueViewAll', error: error.toString() })
    done(null, { statusCode: 500, data: null, message: InternalError })
  }
})
