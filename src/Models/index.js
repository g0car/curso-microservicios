const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize')

const UserModel = sequelize.define('user', {
  name: { type: DataTypes.STRING },
  age: { type: DataTypes.BIGINT },
  color: { type: DataTypes.STRING },
})

const SyncDB = async () => {
  try {
    await UserModel.sync()
  } catch (error) {
    console.log(error)
  }
}

const db = [
  {
    id: 1,
    color: 'Black',
    info: {
      name: 'Jovan',
      age: 18,
      address: 'Filiberto Navas',
      nacionality: 'Mexicano',
      editor: 'VSC',
    },
  },
  {
    id: 2,
    color: 'Gray',
    info: {
      name: 'Thomas Shelby',
      age: 32,
      address: 'Londres',
      nacionality: 'Inglesa',
      editor: 'VSC',
    },
  },
  {
    id: 3,
    color: 'Purple',
    info: {
      name: 'Daniel',
      age: 23,
      address: 'Lago Caimanero',
      nacionality: 'Mexicano',
      editor: 'VSC',
    },
  },
]

module.exports = { SyncDB, UserModel }
