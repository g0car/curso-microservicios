const userController = require('../controllers')

const Create = async ({ user }) => {
  try {
    const { statusCode, data, message } = await userController.Create(user)
    return { statusCode, data, message }
  } catch (error) {
    console.log({ service: 'service Create', error: error.toString() })
    return { statusCode: 500, data: null, message: error.toString() }
  }
}
const Delete = async ({ id }) => {
  try {
    const { statusCode, data, message } = await userController.Delete({ id })
    return { statusCode, data, message }
  } catch (error) {
    console.log({ service: 'service Delete', error: error.toString() })
    return { statusCode: 500, data: null, message: error.toString() }
  }
}
const Update = async ({ user }) => {
  try {
    const { statusCode, data, message } = await userController.Update(user)
    return { statusCode, data, message }
  } catch (error) {
    console.log({ service: 'service Update', error: error.toString() })
    return { statusCode: 500, data: null, message: error.toString() }
  }
}
const FindOne = async ({ id }) => {
  try {
    const { statusCode, data, message } = await userController.FindOne({ id })
    return { statusCode, data, message }
  } catch (error) {
    console.log({ service: 'service FindOne', error: error.toString() })
    return { statusCode: 500, data: null, message: error.toString() }
  }
}
const ViewAll = async () => {
  try {
    const { statusCode, data, message } = await userController.ViewAll()
    return { statusCode, data, message }
  } catch (error) {
    console.log({ service: 'service ViewAll', error: error.toString() })
    return { statusCode: 500, data: null, message: error.toString() }
  }
}

module.exports = {
  Create,
  Delete,
  Update,
  FindOne,
  ViewAll,
}
