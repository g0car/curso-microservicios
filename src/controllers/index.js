const { UserModel } = require('../Models')

const Create = async ({ name, age, color }) => {
  try {
    const user = await UserModel.create({ name, age, color }, { fields: ['name', 'age', 'color'] })

    return { statusCode: 200, data: user.toJSON() }
  } catch (error) {
    console.log({ step: 'controller Create', error: error.toString() })
    return { statusCode: 400, message: error.toString() }
  }
}
const Delete = async ({ id }) => {
  try {
    const user = await UserModel.destroy({ where: { id } })
    console.log(user)
    return { statusCode: 200, data: 'OK' }
  } catch (error) {
    console.log({ step: 'controller Delete', error: error.toString() })
    return { statusCode: 400, message: error.toString() }
  }
}
const Update = async ({ id, name, age, color }) => {
  try {
    const user = await UserModel.update({ name, age, color }, { where: { id } })

    return { statusCode: 200, data: user[1][0].toJSON() }
  } catch (error) {
    console.log({ step: 'controller Update', error: error.toString() })
    return { statusCode: 400, message: error.toString() }
  }
}
const FindOne = async ({ id }) => {
  try {
    const user = await UserModel.findOne({ where: { id } })
    return { statusCode: 200, data: user.toJSON() }
  } catch (error) {
    console.log({ step: 'controller FindOne', error: error.toString() })
    return { statusCode: 500, message: error.toString() }
  }
}
const ViewAll = async () => {
  try {
    const users = await UserModel.findAll()
    return { statusCode: 200, data: users.toJSON() }
  } catch (error) {
    console.log({ step: 'controller View', error: error.toString() })
    return { statusCode: 500, message: error.toString() }
  }
}

module.exports = {
  Create,
  Delete,
  Update,
  FindOne,
  ViewAll,
}
