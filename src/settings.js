const dotenv = require('dotenv')
const { Sequelize } = require('sequelize')
dotenv.config()

const redis = {
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
}

const db_config = {
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  database: process.env.POSTGRES_DB,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  dialect: 'postgres',
}

const InternalError = 'No podemos procesar tu solicitud en estos momentos'

const sequelize = new Sequelize(db_config)

module.exports = { redis, InternalError, sequelize }
