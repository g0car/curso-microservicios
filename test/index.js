const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueViewAll } = require('../src/Adapters')

const Create = async () => {
  try {
    const job = await queueCreate.add({
      color: 'Black',
      name: 'Jovan',
      age: 18,
    })
    const result = await job.finished()
    console.log(result)
    if (result.statusCode === 200) console.log('Solicitud existosa')
  } catch (error) {
    console.log(error)
  }
}
const Delete = async () => {
  try {
    const job = await queueDelete.add({
      id: 2,
    })
    const result = await job.finished()
    console.log(result)
    if (result.statusCode === 200) console.log('Solicitud existosa')
  } catch (error) {
    console.log(error)
  }
}
const Update = async () => {
  try {
    const job = await queueUpdate.add({
      id: 1,
      color: 'Black',
      name: 'Jovan Update',
      age: 18,
    })
    const result = await job.finished()
    console.log(result)
    if (result.statusCode === 200) console.log('Solicitud existosa')
  } catch (error) {
    console.log(error)
  }
}
const FindOne = async () => {
  try {
    const job = await queueFindOne.add({
      id: 1,
    })
    const result = await job.finished()
    console.log(result)
    if (result.statusCode === 200) console.log('Solicitud existosa')
  } catch (error) {
    console.log(error)
  }
}
const ViewAll = async () => {
  try {
    const job = await queueViewAll.add()
    const result = await job.finished()
    console.log(result)
    if (result.statusCode === 200) console.log('Solicitud existosa')
  } catch (error) {
    console.log(error)
  }
}

const main = async () => {
  ViewAll()
}

main()
